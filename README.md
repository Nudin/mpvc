mpvc – mpv based player
=======================

mpv is a great multimedia player and with youtube-dl you can play most videos
form the web. MPVC is a simpel script that brings you a REPL to queue URLs and
files for playback in mpv. It has a some additional features, like setting the
playback speed, etc.

I wrote it for my own needs.

Dependencies
------------
* mpv
* youtube-dl
* nq

Install
-------
$ sudo make install

Usage
-----
* Start mpvc in a terminal.
* Copy-paste or drag in URLs or pathes of multimedia-files to play/queue them.
* Enter any number to set the default playback speed
* Enter 'c' to clear the screen and history
* Enter 'e' to start a new queue
* Enter 'f' to start playback in full screen mode
* Enter 'n' to disable full screen mode
* Enter 'k' to kill all mpv instances
* Enter 'r' to reset pulseaudio
* Enter 'q' to stop mpvc
